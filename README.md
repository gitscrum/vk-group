# VKSearch Group

## `URL` type:
- https://vk.com/club116848644
- https://vk.com/ti_nepoverish
- https%3A%2F%2Fvk.com%2Fsearch%3Fc%5Bsection%5D%3Dpeople%26c%5Bgroup%5D%3D116848644

## Example:
```bash
$ curl "localhost:3000/api/?uri=https://vk.com/ti_nepoverish"
```
```json
{
  "protocol":"https:",
  "host":"vk.com",
  "id":"28477986",
  "groupPrefix":"club",
  "href":"https://vk.com/club28477986"
}
```
> `uri` параметр в `query` запросе обязателен

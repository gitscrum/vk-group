const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const url = require('url');
const querystring = require('querystring');
const got = require('got');
var HTMLParser = require('fast-html-parser');
const pkg = require(path.resolve('./package.json'));

const router = new express.Router();

router.use(bodyParser.urlencoded({extended: false}));

router.get('/favicon.ico', (req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

//* https://vk.com/club116848644
// https://vk.com/ti_nepoverish
// .people_module a
//* https%3A%2F%2Fvk.com%2Fsearch%3Fc%5Bsection%5D%3Dpeople%26c%5Bgroup%5D%3D116848644

router.get('/', async (req, res) => {
	if (
		!Object.prototype.hasOwnProperty.call(req.query, 'uri') ||
		url.parse(req.query.uri).host !== 'vk.com'
	) {
		res.status(400);
		res.send('Bad Request');
		return;
	}

	let id;
	const {
			protocol,
			host,
			pathname,
			query,
			groupPrefix = 'club',
			href
		} = url.parse(req.query.uri);

	if (!id && query && query.includes('group')) {
		id = Number(Object.keys(querystring.parse(query)).filter(key => key.includes('group')).map(key => querystring.parse(query)[key]));
	}

	if (!id && pathname && pathname.includes(groupPrefix)) {
		id = pathname.split('/').filter(path => path.includes(groupPrefix)).toString().substring(groupPrefix.length);
	}

	if (!id && href) {
		await got(href)
			.then(response => {
				const root = HTMLParser.parse(response.body);
				id = root.querySelector('a.wi_date').attributes.href.replace(/.*-([0-9].*)_.*/g, (match, group) => group);
			});
	}

	if (!id) {
		res.status(404);
		res.send('Not Found');
		return;
	}

	res.json({
		protocol,
		host,
		id,
		groupPrefix,
		href: url.resolve(`${protocol}//${host}`, id ? `${groupPrefix}${id}` : '')
	});

});

module.exports = router;

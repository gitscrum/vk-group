const http = require('http');
const morgan = require('morgan');
const express = require('express');
const api = require('./routes/api');
const pkg = require('./package.json');

const app = express();


app.use(morgan('dev'));
app.use('/api', api);
app.use((req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});
app.use((err, req, res) => {
	res.status(err.status || 500);
	res.render('error');
});

app.set('port', pkg.config.server.port);

http
	.createServer(app)
	.listen(pkg.config.server.port);
